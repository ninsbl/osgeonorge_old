============
Installation
============

There are noumerous ways to install OSGeonorge.

--------
Intallation with pip
--------

OSGeonorge can be installed with pip either from pypi:

.. code-block:: shell

   python3 -m pip install osgeonorge

or directly from gitlab:

.. code-block:: shell

   python3 -m pip install git+http://gitlab.com/ninsbl/osgeonorge


--------
Intallation from source
--------

.. code-block:: shell

   git clone https://gitlab.com/ninsbl/osgeonorge
   cd osgeonorge
   python3 -m setup.py --install


--------
Intallation with docker
--------

.. code-block:: shell

   docker ... osgeonorge


--------
Dependencies
--------

OSGeonorge depends on a couple of python packages and C/C++
libraries. The following Python packages should are required
for full functionallity. They are automatically installed
using pip.

* numpy
* GDAL>=3.4.2
* requests (could be replaced by build in modules in future)
* psycopg2 (for connections to PostGIS, could be made optional in future)

The GDAL Python package also requires the installation of the
GDAL C/C++ library. For SOSI support kartverkets OpenFYBA
library is required to activate the OGR-SOSI-driver.

