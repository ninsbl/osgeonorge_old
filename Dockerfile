FROM python:3.9-slim
MAINTAINER Stefan Blumentrath

ARG VERSION=v0.1.6
ARG USERNAME=ninsbl
ARG PROJECT=osgeonorge

RUN apt install libgdal-dev python3-gdal python3-numpy python3-psycopg2 python3-requests
# Download package, install package, no cache
RUN pip install --no-cache-dir https://gitlab.com/$USERNAME/$PROJECT/repository/$VERSION/archive.tar.gz

ENTRYPOINT ["list_atom"]
CMD ["list_atom"]
